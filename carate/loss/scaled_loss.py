import torch
import torch.nn as nn

class ScaledMAELoss(nn.Module):
    
    def __init__(self):
        super(ScaledMAELoss, self).__init__()

    def forward(self, inputs, targets, factor = 10000):
        loss = torch.abs(inputs-targets) * factor
        return loss.mean()


class ScaledMSELoss(nn.Module): 

    def __init__(self): 
        super(ScaledMSELoss, self).__init__()

    def forward(self, inputs, targets, factor): 
        loss = (inputs - targets)**2 *  factor
        return loss.mean()