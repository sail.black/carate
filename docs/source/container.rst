.. _Container Section:


Run CARATE as a container
===========================

A Dockerfile is ditributed alongside the repository. For a wheel installation, please 
refer to the :ref:`Installation Section`.

In your working directory, prepare a config folder and copy the Containerfile from 
the repository. The container looks for the config files in this folder

To build the container from the Dockerfile you can run 

.. code-block:: bash

    podman build --tag carate -f ./Containerfile

After the installation process is done you can use the container by logging into it or

.. code-block:: bash

    podman run -it carate

execute commands in the running container  via 

.. code-block:: bash

    podman exec carate your_command
