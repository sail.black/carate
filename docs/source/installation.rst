.. _Installation Section:

Installing CARATE
==================

CARATE is available to install via the `Python Package Index`_.
For full installation instructions, please refer to the `Install Guide`_ in the project README.

.. code-block:: bash
    
    pip install carate


.. _Python Package Index: https://pypi.org/project/carate/
.. _Install Guide: https://codeberg.org/sail.black/carate


You can also run CARATE inside a container. Please refer to the :ref:`Container Section` for a gentle
tutorial into using containers.
