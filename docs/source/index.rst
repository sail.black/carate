.. carate documentation master file, created by
   sphinx-quickstart on Tue Sep 19 14:53:45 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to carate's documentation!
==================================

You made it to the dojo entry. To proceed using CARATE you are invited to take
a look around the documentation. Please read through the documentation to find 
your way araound. 

The project lives from feedback, so feel free to give it. 

.. toctree::
   
   :maxdepth: 3
   :caption: CARATE

   installation
   container
   minimalconfiguration
   analysis
   publication 



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
